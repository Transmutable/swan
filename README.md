# Open hardware source files for the Transmutable Swan™️

These files contain all of the information that you need to construct an elegant, gift-quality case for projects that use the Adafruit Feather standard.

The design files are licensed under the [CERN Open Hardware Licence Version 2 - Strongly Reciprocal](https://ohwr.org/cern_ohl_s_v2.txt).

*This license has requirements about using and sharing so please read [the license](LICENSE.txt) before using these files.*

## What's in this repo

There are two printed parts (the case and the faceplate) which are defined by a single FreeCAD project (Print.FCStd) and that was used to export two STL files (Body.stl and Faceplate.stl). The design targets resin printers using soybean-based resin (aka "eco-resin") but might also work with different resins or on a filament printer.

There is one circuit board design that is defined by KiCAD files in `/board/`. Follow the directions of your PCB manufacturer to export plots from KiCAD.

The [Bill of Materials](BOM.md) lists the other parts that make up the completed case.
